# Germania Sacra Frontend
This is a standalone client application for Germania Sacra built with VueJS. 

## Local installation

### Requirements:

* NodeJS >= 12 && < 15

### Steps

* Clone the repository to your projects directory `git clone git@gitlab.gwdg.de:subugoe/adw/germania-sacra-frontend.git`
* `cd germania-sacra-frontend`
* Run `npm install`
* Run `npm run serve`
* Access the app on [http://localhost:8080](http://localhost:8080). This will not show any data since there is no local backend running yet.
* Backend:
    - Either clone the [backend repo](https://gitlab.gwdg.de/subugoe/adw/germania-sacra) and follow the steps in the README
    - Or change the base URL of the API in `.env.development` file. For example dev server: 
    `https://api.dev.gs.sub.uni-goettingen.de/v1`.
      
### Build Element UI theme
You can build a new theme CSS file with this command:
```
./node_modules/.bin/et -c src/scss/01-helpers/variables.scss -o src/assets/element-ui -m
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Tests
* Run all tests (unit and e2e in headless mode) `npm run test`
* Run unit tests `npm run test:unit`
* Run e2e tests with browser `npm run test:e2e`
* Run e2e tests with console only `npm run teste2e-headless`

## How to contribute 

This project uses Git hooks to ensure a well-formated commit message. 
If you run `git commit` the Husky hooks `pre-commit` and `prepare-commit-msg` will run. 
The hook `pre-commit` will run unit tests and `prepare-commit-msg` will promt 
an interactive tool to create a commit message (Commitizen). 

## Release

There are multiple enviroments where this project can be deployed to. 

### Dev server
Each push on `develop` branch will deploy to the [dev server](https://dev.gs.sub.uni-goettingen.de)

* `git checkout develop`
* `git pull`
* `git commit`
* `git push`

### Stage server
Each tag on `develop` branch will deploy to the [stage server](https://stage.gs.sub.uni-goettingen.de)

* `git checkout develop`
* `git pull`
* `git commit`
* `npm run changelog`
* `git push --follow-tags`

### Live server
Each push on `main` branch will deploy to the [live server](https://gs.sub.uni-goettingen.de)

* `git checkout main`
* `git pull`
* `git merge --ff develop`
* `git push --follow-tags`
