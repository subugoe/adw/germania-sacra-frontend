import Colors from './src/utils/colors.js'

export default {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  theme: {
    extend: {
      colors: {
        ...Colors,
      },
      container: {
        padding: '2rem',
      },
    },
  },
  variants: {
    margin: ['responsive', 'first', 'last'],
  },
  // plugins: [
  //   import('tailwind-bootstrap-grid')({
  //     gridGutterWidth: '2rem',
  //   }),
  // ],
};
