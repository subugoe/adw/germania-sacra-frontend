module.exports = {
  moduleNameMapper: {
    '\\.(css|less|sass|scss)$': '<rootDir>/tests/mocks/style.mock.js',
    '^tests/(.*)$': '<rootDir>/tests/$1',
  },
  transformIgnorePatterns: ['/node_modules/(?!ol)'],
  preset: '@vue/cli-plugin-unit-jest',
};
