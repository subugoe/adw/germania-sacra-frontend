# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.5.1](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.5.0...v2.5.1) (2025-01-28)


### Bug Fixes

* map button visible in detail view ([0cddcce](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/0cddcce0770bae90e13a636734ae0b7e630ac14f))

## [2.5.0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.4.0...v2.5.0) (2025-01-23)


### Features

* add histogram to map view ([b7a22a8](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/b7a22a82448180cb4c78c967ae6511c47db126df))
* make histogram closable ([c870579](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/c8705795cc5b44e9a85805203b1e41e92a325c01))

## [2.4.0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.3.0...v2.4.0) (2025-01-09)


### Features

* add filter functionality to histogram ([658613d](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/658613db46cfa3044d34df1ef2c17b65b3396e97))


### Bug Fixes

* exclude selected timerange values from histogram request ([a83374c](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/a83374c6e4fe7141a4cc6075c5225dfe121b77e9))
* set max value as default for right input field ([affd2c7](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/affd2c73e614255479b342b0ce8265418f96a411))

## [2.3.0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.2.1...v2.3.0) (2024-11-19)


### Features

* add histogram that displays the timeline of the search results ([4d0382d](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/4d0382dd7ee8b796910d2e145dde4d65abfce5fa))


### Bug Fixes

* make pagination layout more compact on mobile screens to avoid row break ([1d949db](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/1d949dbae1380d6e82b6dcbf9bd65e1c07dbeea8))
* prevent overlow of persons tabs on extra small screens ([970b539](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/970b539e6666860eb91447f0304d88b006e76398))
* re-add frontend version on mobile ([057363f](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/057363fa130a463a9cbeba795a11db7398e7af34))

### [2.2.1](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.2.0...v2.2.1) (2024-10-23)


### Bug Fixes

* re-add functionality to mobile pagination buttons ([0ae02d3](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/0ae02d3c722324db05e0833598b6851ed9c6472a))

## [2.2.0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.1.0...v2.2.0) (2024-10-22)


### Features

* add link to diocese wiag norm data in detail view ([1230b09](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/1230b092bbd4f53cf339023ae33f1915c3c58aa6))


### Bug Fixes

* add target blank to wiag link ([a57e25b](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/a57e25ba8179e806858e86e7bcc10df1097b7142))

## [2.1.0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.0.3...v2.1.0) (2024-10-04)


### Features

* keep facets when changing filters and filter suggestions by selected facets ([1a77532](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/1a7753249acb419558d9d769daf47b9a2157f4d5))


### Bug Fixes

* re-add punctuation marks to citation in details view ([20b4565](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/20b45657d9efb236defe0ac1fdd5e0e6e52022b7))
* update persons url to https in details view ([3c2a3be](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/3c2a3be4f1028c91f6d9b178827f6379a1046835))


### Refactoring

* remove unneeded log ([e0ee606](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/e0ee6066d16d6f1efd0a2c1b68b593d045656a49))

### [2.0.3](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.0.2...v2.0.3) (2024-09-13)


### Bug Fixes

* detail view previous and next button not activated for two items clicking next then prev, next button showing for singular item ([0750706](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/07507060c4df346051c495ae77dc364229a3749d))
* group persons by designations again and update the persons url to the new url ([0bbc97a](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/0bbc97aa530a961699611b09e80dc41a0ae8a9d9))
* next/prev buttons in the detail view are no longer disabled ([d918680](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/d91868004efe67221c991355f6d7ac27ffefe11f))
* optimize map by using default values instead of fixed tile url and null cross origin ([ecd4949](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/ecd4949fb926253e05f509b5d367af39c2891f4a))
* reset all buton not actually resetting search params on next search, reset all button still showing even if all fields are empty after local facet reset ([9a4b863](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/9a4b8630ee1faa031187c95b51769a043a26fced))
* show (german) no result message only for input fields with showSuggestions when no results are found, differentiate keyword and suggestion search selection, restore reset button and restore input fields without suggestions ([24b27a6](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/24b27a6ba587216b68c92fbd23b31b626454d46f))
* update searchgroups vue-multiselect to primevue select, show correct icon, clear value field if select changes ([cac707e](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/cac707ea57e5da448a99797df1762b87b3cf5bfc))
* update tile source url to eliminate 301 redirects ([f8da58e](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/f8da58ecc68beabe47eef6fa3ed914bd54b04699))

### [2.0.2](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.0.1...v2.0.2) (2024-09-09)


### Bug Fixes

* add max width to filter section to avoid overflow ([f9dc5cc](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/f9dc5ccb7caa4c783ad75e450af824d39b2c2c12))
* adjust appearance of primevue checkbox options to match the styling of the custom checkbox options ([d971cbe](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/d971cbe2d4d1bafbedd80f8d0ad50c23873e680c))
* also show place names in mobile view again after API change ([51c4088](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/51c40882d05bd69b9a76625e0393b181ef593542))
* keep search filters and facets between list and map view ([c826cbb](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/c826cbb4525e9403d8af8ed79e250c10f0ab5682))
* rename facet emit to avoid listening to native change event ([16241c3](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/16241c3413c87c876fb8d0cea5e52ad531a308cd))
* trigger a search when the mouse loses focus ([c08879e](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/c08879e75b305a182c9bc48ec1fa4b4b0f37f733))

### [2.0.1](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v2.0.0...v2.0.1) (2024-08-16)


### Bug Fixes

* display front-end app version ([9efc9cf](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/9efc9cf2f23a70275fb092db06aff68bcbe78309))

## [2.0.0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v1.2.0...v2.0.0) (2024-08-16)


### ⚠ BREAKING CHANGES

* update app to Vue 3, replace Element UI components with PrimeVue

### Features

* add custom map controls for diocese borders and clusters ([4aa909b](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/4aa909bec8ae24cb5cc1a09c1b3d2aa78164053e))
* show either monastery location or place coordinates with a label in detail view ([557e596](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/557e5962957bf8203a0785aea23c0fd2e163a3bc))
* update app to Vue 3, replace Element UI components with PrimeVue ([5c26e0b](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/5c26e0b2df2263ab9b24738ef49b617cb843c87f))


### Bug Fixes

* move list map toggle to the middle ([ced24ca](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/ced24ca5ca59704bc5e7ad61995cced7a5bb7991))
* show place names after API change in list again ([a387a7e](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/a387a7e9750c3d38e61b49ba7fbad8f09b5f9355))
* update permission on restart script ([bd9593e](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/commit/bd9593e581ce0a7ad82a45e538f1e0b2f3250981))

## [1.2.0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v1.1.1...v1.2.0) (2022-11-02)


### Features

* add fonts as local assets ([6163c72](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/-/commit/6163c72b0569750fe83b0eaced7ebf5202a4fe78))


### Chore

* update readme with release info ([756326b](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/-/commit/756326b2afbfa45d5b98d8591d62ef53ade6d3e7))

### [1.1.1](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/compare/v1.0.7...v1.1.1) (2021-10-05)


### Chore

* add husky hooks ([f0965c0](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/-/commit/f0965c0b7b51931b7b7ba60ffe607a10af47ca4c))
* add standard-version ([a60431b](https://gitlab.gwdg.de/subugoe/adw/germania-sacra-frontend/-/commit/a60431b07867102c159a2018efe6f980f87298f2))
