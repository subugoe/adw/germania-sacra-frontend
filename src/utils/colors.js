import colors from 'tailwindcss/colors';

export default {
  primary: {
    50: '#fff2f3',
    100: '#f0ccd1',
    200: '#de7c89',
    300: '#de374e',
    400: '#cc1830',
    500: '#b3152a',
    600: '#911a2a',
    700: '#731521',
    800: '#5c111b',
    900: '#380a10',
    950: '#21070b'
  },
  gray: {
    ...colors.neutral
  }
};
