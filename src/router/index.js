import {createMemoryHistory, createRouter, createWebHistory} from 'vue-router';
import MonasteryDetail from '@/components/MonasteryDetail.vue';
import MonasteriesListFactory from '@/components/MonasteriesListFactory.vue';
import Monasteries from '@/pages/Monasteries.vue';

export const routes = [
  {
    path: '/',
    component: Monasteries,
    alias: '/liste',
    children: [
      {
        path: '',
        component: MonasteriesListFactory,
      },
    ],
  },
  {
    path: '/karte',
    component: Monasteries,
    children: [
      {
        path: '',
        component: MonasteriesListFactory,
      },
    ],
  },
  {
    path: '/gsn/:id',
    component: Monasteries,
    children: [
      {
        path: '',
        component: MonasteryDetail,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.VITE_APP_BASE_URL),
  routes,
})

export default router;
