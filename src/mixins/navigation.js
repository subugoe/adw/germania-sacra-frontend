export default {
  links: [
    {
      label: 'About',
      url: 'https://adw-goe.de/germania-sacra/klosterdatenbank/',
      external: true
    },
    {
      label: 'Datenbestand',
      url: 'https://adw-goe.de/germania-sacra/klosterdatenbank/datenbestand/',
      external: true
    },
    {
      label: 'Datenschutzerklärung',
      url: 'https://adw-goe.de/ueber-uns/datenschutzerklaerung/',
      external: true
    },
    {
      label: 'Impressum',
      url: 'https://adw-goe.de/germania-sacra/impressum/',
      external: true
    },
  ],
};
