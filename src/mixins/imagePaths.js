export const imagePaths = {
  getIcon (orderName) {
    const imageName = orderName ?? 'default_monastery';
    return import.meta.env.VITE_APP_BASE_URL_ASSETS + '/icons/' + imageName + '.png';
  },
  getMarker (orderName) {
    const imageName = orderName ?? 'default_monastery';
    return import.meta.env.VITE_APP_BASE_URL_ASSETS + '/markers/' + imageName + '.png';
  },
  getMarkerTemplate () {
    return '/assets/images/Kloster_template.png';
  },
};
