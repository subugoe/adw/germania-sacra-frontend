import resolveConfig from 'tailwindcss/resolveConfig';
import tailwindConfig from '@/../tailwind.config.js';

const { theme } = resolveConfig(tailwindConfig);
const themeScreens = {};
Object.keys(theme.screens).map(key => {
  themeScreens[key] = parseInt(theme.screens[key].replace('px', ''));
});
export const screens = {
  screens: themeScreens,
  isMobile () {
    return window.innerWidth < themeScreens.lg;
  },
};
