import { createApp } from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import PrimeVue from 'primevue/config';
import Aura from '@primevue/themes/aura';
import 'primeicons/primeicons.css'

import moment from 'moment';
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import Paginator from "primevue/paginator";
import {definePreset} from "@primevue/themes";
import Popover from "primevue/popover";
import Button from "primevue/button";
import SelectButton from "primevue/selectbutton";
import Checkbox from "primevue/checkbox";
import Tag from "primevue/tag";
import Card from "primevue/card";
import Drawer from "primevue/drawer";
import Dialog from "primevue/dialog";
import Skeleton from "primevue/skeleton";

import Colors from '@/utils/colors.js'
import AutoComplete from "primevue/autocomplete";
import FloatLabel from "primevue/floatlabel";
import ToggleSwitch from "primevue/toggleswitch";

// Vue.config.productionTip = false;

// moment.locale('de');

const app = createApp(App);

const GSAura = definePreset(Aura, {
  semantic: {
    primary: Colors.primary,
    colorScheme: {
      light: {
        surface: Colors.gray
      }
    },
  },
  components: {
    paginator: {
      root: {
        background: 'none'
      }
    },
    checkbox: {
      root: {
        width: '18px',
        height: '18px',
        hover: {
          border: { color: '{primary.500}'}
        }
      }
    }
  }
})

app.use(PrimeVue, {
  theme: {
    preset: GSAura,
    options: {
      darkModeSelector: '.app-dark'
    }
  },
});
app.use(router);
app.use(store);

app.component('DataTable', DataTable);
app.component('Column', Column);
app.component('Paginator', Paginator);
app.component('Popover', Popover);
app.component('Button', Button);
app.component('SelectButton', SelectButton);
app.component('Checkbox', Checkbox);
app.component('Tag', Tag);
app.component('Card', Card);
app.component('Drawer', Drawer);
app.component('Dialog', Dialog);
app.component('Skeleton', Skeleton);
app.component('AutoComplete', AutoComplete);
app.component('FloatLabel', FloatLabel);
app.component('ToggleSwitch', ToggleSwitch);

app.mount('#app');

