import axios from 'axios';

class ApiService {
  constructor (baseURL) {
    this.http = axios.create({
      baseURL: baseURL,
      timeout: 5000,
    });
    this.http.interceptors.response.use(
      (response) => response.data,
      (error) => {
        console.log(error);
        return Promise.reject(error);
      },
    );
  }

  async getMonasteriesList (params) {
    return this.request('monasteries/list', 'GET', null, params);
  }

  async getMonasteriesLocations (params) {
    return this.request('monasteries/locations', 'GET', null, params);
  }

  async getMonasteriesGeoJSON (params) {
    return this.request('monasteries/locations/geojson', 'GET', null, params);
  }

  async getMonasteriesFacet (field, params) {
    return this.request('monasteries/facet/' + field, 'GET', null, params);
  }

  async getMonasteriesFacets (params) {
    return this.request('monasteries/facets', 'GET', null, params);
  }

  async getHistogram (params) {
    return this.request('monasteries/histogram', 'GET', null, params);
  }

  async getSuggestions (params) {
    return this.request('monasteries/suggest', 'GET', null, params);
  }

  async getDioceseBorders (params) {
    return await this.request('/monasteries/diocese-borders', 'GET', null, params);
  }

  async getMonasteryDetail (id) {
    return await this.request('/monastery/' + id, 'GET');
  }

  async getOrders () {
    return await this.request('/orders', 'GET', null);
  }

  async getMonasteryStatuses () {
    return await this.request('/monasteries/statuses', 'GET', null);
  }

  async getVersion () {
    return await this.request('/version', 'GET', null);
  }

  async request (url, method, data, params) {
    return await this.http.request({
      url,
      method,
      data,
      params,
      paramsSerializer: this.serializeParams,
      timeout: 10000,
    });
  }

  serializeParams (obj) {
    // https://stackoverflow.com/a/56174085/1959760
    const getPairs = (obj, keys = []) =>
      Object.entries(obj).reduce((pairs, [key, value]) => {
        if (typeof value === 'object') { pairs.push(...getPairs(value, [...keys, key])) } else { pairs.push([[...keys, key], value]) }
        return pairs;
      }, []);

    const queryString = getPairs(obj)
      .map(([[key0, ...keysRest], value]) =>
        `${key0}${keysRest.map(a => `[${a}]`).join('')}=${value}`)
      .join('&');

    return queryString;
  }
}

export default new ApiService(import.meta.env.VITE_APP_BASE_URL_API);
