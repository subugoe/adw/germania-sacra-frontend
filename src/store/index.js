import {createStore} from 'vuex';
import apiService from '@/store/api';
import types from '@/store/mutation-types';
import paramsService from '@/store/params';

function createFacetQuery (facet, field) {
  return facet.active?.length 
    > 0 ? 
    facet.active.map(active => ({
      field,
      value: active,
      operator: 'eq',
    }))
    :
    null; //reset facetQuery if local reset button was clicked
}

const defaultQuery = {
  search: null,
  facetDiocese: null,
  facetOrder: null,
  facetGender: null,
  limit: 30,
  offset: 0,
}

const state = {
  isMobile: false,
  view: 'list',
  page: 0,
  histogram: [],
  histogramOptions: {
    start: null,
    end: null,
    gap: 1
  },
  initialTimeRange: {
    start: null,
    end: null
  },
  monasteriesList: [],
  monasteriesListTotalNumber: 0,
  monasteryDetail: null,
  monasteryStatuses: [],
  monasteriesQuery: { ...defaultQuery },
  facetDiocese: null,
  facetOrder: null,
  facetGender: null,
  disableFacets: false,
  locations: [],
  orderSymbolsMap: {},
  loadingList: false,
  loadingLocations: true,
  loadingHistogram: false,
  loadingFacetDiocese: false,
  loadingFacetOrder: false,
  loadingFacetGender: false,
  loadingDetail: false,
  loadedList: false,
  loadedFacetDiocese: false,
  loadedFacetOrder: false,
  loadedFacetGender: false,
  loadedLocations: false,
  loadedHistogram: false,
  loadedDetail: false,
  errors: {
    list: null,
    map: null,
    detail: null,
  },
  apiVersion: null,
};

const getters = {
  getLoadingFacet: (state) => (field) => {
    let loadingFacet;
    if (field === 'diocese') {
      loadingFacet = state.loadingFacetDiocese;
    } else if (field === 'order') {
      loadingFacet = state.loadingFacetOrder;
    } else if (field === 'gender') {
      loadingFacet = state.loadingFacetGender;
    }
    return loadingFacet;
  },
};

export const actions = {
  search ({ commit, dispatch, state }, payload = { searchQuery: null, keepFacets: false} ) {
    const searchQuery = payload.searchQuery?.length > 0 ? payload.searchQuery : null;
    const keepFacets = payload.keepFacets; 
    const facetsActive = {};

    if (keepFacets) {
      facetsActive['diocese'] = state.facetDiocese?.active;
      facetsActive['order'] = state.facetOrder?.active;
      facetsActive['gender'] = state.facetGender?.active;
    }

    const query = {
      search: searchQuery,
      facetDiocese: null,
      facetOrder: null,
      facetGender: null,
      offset: 0,
    };

    dispatch('setQuery', query);

    commit(types.SET_LOADED_LOCATIONS, false);
    commit(types.SET_LOADED_LIST, false);

    let anyFacetsApplied = false;
    if (keepFacets) {
      for (const [field, values] of Object.entries(facetsActive)) {
        if (values?.length > 0) {
          dispatch('applyFacet', { values, field});
          anyFacetsApplied = true;
        }
      } 
    }
    if (!keepFacets || !anyFacetsApplied) {
      if (state.view === 'list') {
        dispatch('getMonasteriesList');
      } else if (state.view === 'map') {
        dispatch('getMonasteriesLocations');
      }
      commit(types.SET_FACET_DIOCESE, null);
      commit(types.SET_FACET_ORDER, null);
      commit(types.SET_FACET_GENDER, null);  
    }
    dispatch('setFacetsDisabled', false);
    dispatch('getMonasteriesFacets');  

    dispatch('getHistogram'); 
  },
  async getSuggestions ({ state }, suggestQuery) {
    let suggestions = [];
    const params = paramsService.mapSuggestQuery(state.monasteriesQuery, suggestQuery);
    try {
      const response = await apiService.getSuggestions(params);
      suggestions = response.suggestions;
    } catch (error) {
      console.log(error);
    }
    return suggestions;
  },
  async applyFacet ({ commit, dispatch, state }, { values, field }) {
    let facet;
    let query;
    let reloadAttributes = [];

    if (field === 'diocese') {
      facet = state.facetDiocese;
      facet.active = values;
      query = { facetDiocese: createFacetQuery(facet, field) };
      reloadAttributes = ['order', 'gender'];
    } else if (field === 'order') {
      facet = state.facetOrder;
      facet.active = values;
      query = { facetOrder: createFacetQuery(facet, field) };
      reloadAttributes = ['diocese', 'gender'];
    } else if (field === 'gender') {
      facet = state.facetGender;
      facet.active = values;
      query = { facetGender: createFacetQuery(facet, field) };
      reloadAttributes = ['diocese', 'order'];
    }

    if (values.length === 0) {
      // Add current facet to reload when when clicked on reset
      reloadAttributes.push(field);
    }

    dispatch('setQuery', { ...query, offset: 0 });

    commit(types.SET_LOADED_LOCATIONS, false);
    commit(types.SET_LOADED_LIST, false);

    dispatch('getMonasteriesFacets', reloadAttributes);
    dispatch('getHistogram');
    if (state.view === 'list') {
      dispatch('getMonasteriesList');
    } else if (state.view === 'map') {
      dispatch('getMonasteriesLocations');
    }
  },

  async resetAllFilters ({ dispatch }) {
    dispatch('search');
  },

  async getMonasteriesList ({ commit, dispatch, state }, query) {
    if (query) {
      dispatch('setQuery', query);
    }
    dispatch('setFacetsDisabled', true);
    let loaded = false;
    try {
      commit(types.SET_LOADING_LIST, true);
      const response = await apiService.getMonasteriesList(paramsService.mapQuery(state.monasteriesQuery));
      commit(types.SET_MONASTERIES_LIST, response.list);
      commit(types.SET_MONASTERIES_LIST_TOTAL_NUMBER, response.total);
      commit(types.SET_LOADING_LIST, false);
      loaded = true;
    } catch (error) {
      console.log(error);
      commit(types.SET_LOADING_LIST, false);
    }
    commit(types.SET_LOADED_LIST, loaded);
    dispatch('setFacetsDisabled', false);
    return loaded;
  },

  async getMonasteriesLocations ({ commit, dispatch, state }, payload = { query: null, silent: false }) {
    const query = payload.query;
    const silent = payload.silent;

    if (query && !silent) {
      dispatch('setQuery', query);
    }
    dispatch('setFacetsDisabled', true);
    let loaded = false;
    try {
      if (!silent) commit(types.SET_LOADING_LOCATIONS, true);
      const response = await apiService.getMonasteriesGeoJSON(paramsService.mapQuery(state.monasteriesQuery));
      commit(types.SET_LOCATIONS, response.features);

      if (!silent) commit(types.SET_LOADING_LOCATIONS, false);
      loaded = true;
    } catch (error) {
      console.log(error);
      if (!silent) commit(types.SET_LOADING_LOCATIONS, false);
    }
    commit(types.SET_LOADED_LOCATIONS, loaded);
    dispatch('setFacetsDisabled', false);
    return loaded;
  },

  async getMonasteriesFacet ({ commit, dispatch, state }, field) {
    let typeLoading;
    let typeLoaded;
    let typeSet;
    let active;
    let query; //ensures independency of active (selected) values and all values that can be selected

    if (field === 'diocese') {
      typeLoading = types.SET_LOADING_FACET_DIOCESE;
      typeLoaded = types.SET_LOADED_FACET_DIOCESE;
      typeSet = types.SET_FACET_DIOCESE;
      active = state.facetDiocese ? state.facetDiocese.active : [];
      query = { ...state.monasteriesQuery, facetDiocese: null};
    } else if (field === 'order') {
      typeLoading = types.SET_LOADING_FACET_ORDER;
      typeLoaded = types.SET_LOADED_FACET_ORDER;
      typeSet = types.SET_FACET_ORDER;
      active = state.facetOrder ? state.facetOrder.active : [];
      query = { ...state.monasteriesQuery, facetOrder: null}
    } else if (field === 'gender') {
      typeLoading = types.SET_LOADING_FACET_GENDER;
      typeLoaded = types.SET_LOADED_FACET_GENDER;
      typeSet = types.SET_FACET_GENDER;
      active = state.facetGender ? state.facetGender.active : [];
      query = { ...state.monasteriesQuery, facetGender: null}
    }
    let loaded = false;
    try {
      commit(typeLoading, true);
      const response = await apiService.getMonasteriesFacet(field, paramsService.mapQuery(query));
      commit(typeSet, { ...response, active: active });
      commit(typeLoading, false);
      loaded = true;
    } catch (error) {
      console.log(error);
      commit(typeLoading, false);
    }
    commit(typeLoaded, loaded);
    return loaded;
  },

  async getMonasteriesFacets ({ commit, dispatch }, attributes = ['diocese', 'order', 'gender']) {
    attributes.forEach(field => {
      dispatch('getMonasteriesFacet', field);
    });
  },

  async getHistogram ({ commit, dispatch }, query) {
    const isDefaultQuery = (query) => {
      return Object.keys(defaultQuery).every(key => query[key] === defaultQuery[key])
    }

    if (!query) {
      query = { 
        ...state.monasteriesQuery,
        search: (
          state.monasteriesQuery.search 
            ? state.monasteriesQuery.search.filter((item) => item.field !== 'dissolutionAfter' && item.field !== 'foundationBefore')
            : null
        )
      };
    }
    let loaded = false;
    try {
      commit(types.SET_LOADING_HISTOGRAM, true);
      const response = await apiService.getHistogram(paramsService.mapHistogramQuery(query, state.histogramOptions));
      commit(types.SET_HISTOGRAM, response.histogram);
      if (isDefaultQuery(query)) {
        dispatch('setInitialTimeRange', {
          start: parseInt(response.histogram[0].startLabel), 
          end: parseInt(response.histogram[response.histogram.length - 1].startLabel) + 1
        })
      }
      commit(types.SET_LOADING_HISTOGRAM, false);
      loaded = true;
    } catch (error) {
      console.log(error);
      commit(types.SET_LOADING_HISTOGRAM, false);
    }
    commit(types.SET_LOADED_HISTOGRAM, loaded);
    return loaded;
  },

  async getDetail ({ commit, state, dispatch }, id) {
    try {
      commit(types.SET_LOADING_DETAIL, true);
      commit(types.SET_ERROR_DETAIL, null);

      if (!state.loadedLocations) {
        dispatch('getMonasteriesLocations');
      }

      const response = await apiService.getMonasteryDetail(id);

      commit(types.SET_LOADING_DETAIL, false);
      commit(types.SET_MONASTERY_DETAIL, response);
    } catch (e) {
      commit(types.SET_LOADING_DETAIL, false);
      commit(types.SET_ERROR_DETAIL, e);
    }
  },

  async getMonasteryStatuses ({ commit }) {
    try {
      const response = await apiService.getMonasteryStatuses();
      commit(types.SET_MONASTERY_STATUSES, response);
    } catch (e) {
    }
  },

  async getDioceseBorders () {
    let geojson = '';
    try {
      geojson = await apiService.getDioceseBorders();
    } catch (error) {
      console.log(error);
    }
    return geojson;
  },

  async getOrderSymbols ({ commit }) {
    try {
      const response = await apiService.getOrders();
      const orderSymbolsMap = {};
      response.orders.forEach(order => {
        const symbol = order.symbol || 'Kloster_allgemein.svg';
        orderSymbolsMap[order.name] = symbol.split('.')[0];
      });
      commit(types.SET_ORDER_SYMBOLS, orderSymbolsMap);
    } catch (error) {
      console.log(error);
    }
  },
  async getApiVersion ({ commit }) {
    try {
      const response = await apiService.getVersion();
      commit(types.SET_API_VERSION, response.version);
    } catch (error) {
      console.log(error);
    }
  },
  setQuery ({ commit, state }, query) {
    commit(types.SET_MONASTERIES_QUERY, query);
  },
  setView ({ commit }, view) {
    commit(types.SET_VIEW, view);
  },
  setIsMobile ({ commit }, isMobile) {
    commit(types.SET_IS_MOBILE, isMobile);
  },
  setPage ({ commit }, page) {
    commit(types.SET_PAGE, page);
  },
  setFacetsDisabled ({ commit }, disabled) {
    commit(types.SET_FACETS_DISABLED, disabled);
  },
  setInitialTimeRange({ commit }, initialTimeRange) {
    commit(types.SET_INITIAL_TIMERANGE, initialTimeRange)
  }
};

export const mutations = {
  [types.SET_IS_MOBILE]: (state, payload) => {
    state.isMobile = payload;
  },
  [types.SET_VIEW]: (state, payload) => {
    state.view = payload;
  },
  [types.SET_PAGE]: (state, payload) => {
    state.page = payload;
  },
  [types.SET_MONASTERIES_LIST]: (state, payload) => {
    state.monasteriesList = payload;
  },
  [types.SET_MONASTERIES_LIST_TOTAL_NUMBER]: (state, payload) => {
    state.monasteriesListTotalNumber = payload;
  },
  [types.SET_MONASTERY_STATUSES]: (state, payload) => {
    state.monasteryStatuses = payload;
  },
  [types.SET_MONASTERY_DETAIL]: (state, payload) => {
    state.monasteryDetail = payload;
  },
  [types.SET_LOCATIONS_ALL]: (state, payload) => {
    state.locationsAll = payload;
  },
  [types.SET_FACET_DIOCESE]: (state, payload) => {
    state.facetDiocese = payload;
  },
  [types.SET_FACET_ORDER]: (state, payload) => {
    state.facetOrder = payload;
  },
  [types.SET_FACET_GENDER]: (state, payload) => {
    state.facetGender = payload;
  },
  [types.SET_FACETS_DISABLED]: (state, payload) => {
    state.disableFacets = payload;
  },
  [types.SET_LOCATIONS]: (state, payload) => {
    state.locations = payload;
  },
  [types.SET_HISTOGRAM]: (state, payload) => {
    state.histogram = payload;
  },
  [types.SET_INITIAL_TIMERANGE]: (state, payload) => {
    state.initialTimeRange = payload;
  },
  [types.SET_MONASTERIES_QUERY]: (state, payload) => {
    state.monasteriesQuery = { ...state.monasteriesQuery, ...payload };
  },
  [types.SET_ORDER_SYMBOLS]: (state, payload) => {
    state.orderSymbolsMap = payload;
  },
  [types.SET_LOADING_LIST]: (state, payload) => {
    state.loadingList = payload;
  },
  [types.SET_LOADING_LOCATIONS]: (state, payload) => {
    state.loadingLocations = payload;
  },
  [types.SET_LOADING_FACET_DIOCESE]: (state, payload) => {
    state.loadingFacetDiocese = payload;
  },
  [types.SET_LOADING_FACET_ORDER]: (state, payload) => {
    state.loadingFacetOrder = payload;
  },
  [types.SET_LOADING_FACET_GENDER]: (state, payload) => {
    state.loadingFacetGender = payload;
  },
  [types.SET_LOADING_HISTOGRAM]: (state, payload) => {
    state.loadingHistogram = payload;
  },
  [types.SET_LOADING_DETAIL]: (state, payload) => {
    state.loadingDetail = payload;
  },
  [types.SET_LOADED_LIST]: (state, payload) => {
    state.loadedList = payload;
  },
  [types.SET_LOADED_LOCATIONS]: (state, payload) => {
    state.loadedLocations = payload;
  },
  [types.SET_LOADED_FACET_DIOCESE]: (state, payload) => {
    state.loadedFacetDiocese = payload;
  },
  [types.SET_LOADED_FACET_ORDER]: (state, payload) => {
    state.loadedFacetOrder = payload;
  },
  [types.SET_LOADED_FACET_GENDER]: (state, payload) => {
    state.loadedFacetGender = payload;
  },
  [types.SET_LOADED_HISTOGRAM]: (state, payload) => {
    state.loadedHistogram = payload;
  },
  [types.SET_LOADED_DETAIL]: (state, payload) => {
    state.loadedDetail = payload;
  },
  [types.SET_ERROR_LIST]: (state, payload) => {
    state.errors.list = payload;
  },
  [types.SET_ERROR_DETAIL]: (state, payload) => {
    state.errors.detail = payload;
  },
  [types.SET_ERROR_MAP]: (state, payload) => {
    state.errors.map = payload;
  },
  [types.SET_API_VERSION]: (state, payload) => {
    state.apiVersion = payload;
  },
};

export default createStore({
  state,
  getters,
  actions,
  mutations,
});
