class UrlParamsService {
  constructor () {
    this.params = null;
  }

  get () {
    this.params = new URLSearchParams(window.location.search);
  }

  set (params) {
    params = this.mapParamsToString(params);
    if (history.pushState) {
      var newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + params;
      window.history.pushState({ path: newurl }, '', newurl);
    }
  }

  mapQuery (searchQuery) {
    const requestParams = this.mapSearchParameter(searchQuery);

    requestParams.limit = searchQuery.limit;
    requestParams.offset = searchQuery.offset;

    return requestParams;
  }

  mapSuggestQuery(searchQuery, { field, value}) {
    const requestParams = this.mapSearchParameter(searchQuery);
    
    requestParams.field = field;
    requestParams.value = value;

    return requestParams;
  }

  mapHistogramQuery(searchQuery, {start, end, gap}) {
    const requestParams = this.mapSearchParameter(searchQuery);
    
    if (start) requestParams.start = start;
    if (end) requestParams.end = end;
    if (gap) requestParams.gap = gap;

    return requestParams;
  }

  mapSearchParameter({ 
    search, 
    facetDiocese, 
    facetOrder, 
    facetGender 
  }) {
    const requestParams = {};
    const filters = [];

    if (!search) search = [];

    if (facetDiocese) filters.push(...facetDiocese);
    if (facetOrder) filters.push(...facetOrder);
    if (facetGender) filters.push(...facetGender);

    search = [...search, ...filters];

    requestParams.q = this.mapQueryToString(search);
    return requestParams;
  }
  
  mapParamsToString (params) {
    return new URLSearchParams(params).toString();
  }

  mapQueryToString (params) {
    const obj = params.map(({ field, value, operator }, i) => ({
      field: field,
      value,
      operator,
    }));

    return obj;
  }
}

export default new UrlParamsService();
