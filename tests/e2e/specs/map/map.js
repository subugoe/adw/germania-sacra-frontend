describe('Map', () => {
  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/locations?limit=30&offset=0', 'fixture:locations.json');
    cy.visit('/monasteries/map');
    cy.wait(500);
  });

  it('loads the map', () => {
    cy.get('#map').should('be.visible');
  });

  it('displays map controls', () => {
    cy.get('.ol-zoom').contains('+');
    cy.get('.ol-zoom').contains('−');
  });
});
