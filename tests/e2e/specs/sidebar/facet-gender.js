function getFacetGender () {
  return cy.get('.facet-gender');
}

function getFacetGenderList () {
  return getFacetGender().find('.el-checkbox-group');
}

function checkSearchResult (amount) {
  return cy.get('.content-container').contains('Suchergebnis').parent().contains(amount + ' Treffer');
}

describe('Facet gender', () => {
  let listMaennerklosterJson;

  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/gender?limit=30&offset=0', 'fixture:facet-gender.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?limit=30&offset=0', 'fixture:list.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=gender.eq:Männerkloster&limit=30&offset=0', 'fixture:list-gender-maennerkloster.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=gender.eq:Frauenkloster&limit=30&offset=0', 'fixture:list-gender-frauenkloster.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=gender.eq:Doppelkloster&limit=30&offset=0', 'fixture:list-gender-doppelkloster.json');
    // TODO: Fix OR relation on gender facet first
    // cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=gender.eq:Frauenkloster,Männerkloster&limit=30&offset=0', 'fixture:list-Männerkloster-Frauenkloster.json');
    cy.fixture('list-gender-maennerkloster.json').then((json) => {
      listMaennerklosterJson = json;
    });
    cy.visit('/monasteries/list');
  });

  it('loads the gender facet', () => {
    getFacetGender().contains('Geschlecht');
  });

  it('checks multiple selection checkbox', () => {
    getFacetGender().contains('Männerkloster').click();
    getFacetGender().contains('Frauenkloster').click();

    getFacetGenderList().contains('Männerkloster').should('have.class', 'is-checked');
    getFacetGenderList().contains('Frauenkloster').should('have.class', 'is-checked');

    getFacetGenderList().contains('Frauenkloster').click();
    getFacetGenderList().contains('Männerkloster').click();

    getFacetGenderList().contains('Frauenkloster').should('not.have.class', 'is-checked');
    getFacetGenderList().contains('Männerkloster').should('not.have.class', 'is-checked');
  });

  it('triggers request on checkbox click', () => {
    getFacetGender().contains('Männerkloster').click();
    getFacetGender().get('.el-loading-spinner').should('be.visible');
    checkSearchResult(listMaennerklosterJson.total);
  });

  it('checks if a list exist ', () => {
    getFacetGenderList().find('.el-checkbox').should('have.length', 4);
  });

  it('resets selection ', () => {
    getFacetGender().contains('Männerkloster').click();

    getFacetGenderList().contains('Frauenkloster').click();

    getFacetGender().contains('zurücksetzen').click();

    getFacetGenderList().contains('Männerkloster').should('not.have.class', 'is-checked');
    getFacetGenderList().contains('Frauenkloster').should('not.have.class', 'is-checked');
  });
});
