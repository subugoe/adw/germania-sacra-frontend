function getSearchContainer () {
  return cy.get('.search-container');
}

function getNumberSearchBarContainer () {
  return getSearchContainer().find('>div:nth-child(3)');
}

function clickInSearchBar () {
  return getNumberSearchBarContainer().first().click();
}

function checkSearchResult (amount) {
  return cy.get('.content-container').contains('Suchergebnis').parent().contains(amount + ' Treffer');
}

describe('Search Number', () => {
  let listGsn20Json;
  let listGsn20Gsn77json;
  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?limit=30&offset=0', 'fixture:list.json');
    cy.route(
      'GET',
      'http://localhost:8000/v1/monasteries/list?q=number.eq:20&limit=30&offset=0',
      'fixture:list-number-gsn20.json',
    );
    cy.route(
      'GET',
      'http://localhost:8000/v1/monasteries/list?q=number.eq:77&limit=30&offset=0',
      'fixture:list-number-gsn77.json',
    );

    cy.fixture('list-number-gsn20.json').then((json) => {
      listGsn20Json = json;
    });
    cy.fixture('list-number-gsn20-gsn77.json').then((json) => {
      listGsn20Gsn77json = json;
    });
    cy.visit('/monasteries/list');
  });

  it('loads the Number search bar', () => {
    getSearchContainer().contains('Klosternummer(GSN), GND, Wikidata etc.');
  });

  it('reveals the input when clicking into search bar', () => {
    clickInSearchBar();
    getNumberSearchBarContainer().find('input[type=text]').should('be.visible');
  });

  it('trigger search on with entered input', () => {
    clickInSearchBar();
    getNumberSearchBarContainer().find('input[type=text]').type('20').type('{enter}');
    getSearchContainer().contains('Suchen').click();
    checkSearchResult(listGsn20Json.total);
  });

  // TODO: Fix OR in multiple entered inputs
  // it('trigger search on with multiple entered inputs', () => {
  //   clickInSearchBar();
  //   getNumberSearchBarContainer().find('input[type=text]').type('20').type('{enter}');
  //   clickInSearchBar();
  //   getNumberSearchBarContainer().find('input[type=text]').type('77').type('{enter}');
  //
  //   getSearchContainer().contains('Suchen').click();
  //   checkSearchResult(listGsn20Gsn77json.total);
  // });

  it('performs search field specific interactions', () => {
    // check click on suggestions
    clickInSearchBar();
    getNumberSearchBarContainer().find('input[type=text]').type('20');
    getNumberSearchBarContainer().find('.multiselect__content').contains('20').click();
    getNumberSearchBarContainer().find('.multiselect__tags').contains('20');

    // check floating label
    getNumberSearchBarContainer().find('.floating-label').contains('Klosternummer(GSN), GND, Wikidata etc.');

    // check reset
    getNumberSearchBarContainer().contains('zurücksetzen').click();
    getNumberSearchBarContainer().find('.multiselect__tags-wrap').should('be.empty');

    // check tag close button
    clickInSearchBar();
    getNumberSearchBarContainer().find('input[type=text]').type('20').type('{enter}');
    getNumberSearchBarContainer().find('.multiselect__tags').contains('20').siblings('.multiselect__tag-icon').click();
    getNumberSearchBarContainer().find('.multiselect__tags-wrap').should('be.empty');
  });
});
