function getSearchContainer () {
  return cy.get('.search-container');
}

function getLocationSearchBarContainer () {
  return getSearchContainer().find('div:first-child');
}

function clickInSearchBar () {
  return getLocationSearchBarContainer().first().click();
}

function checkSearchResult (amount) {
  return cy.get('.content-container').contains('Suchergebnis').parent().contains(amount + ' Treffer');
}

describe('Search Location', () => {
  let listKoelnJson;
  let listKoelnAachenJson;
  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?limit=30&offset=0', 'fixture:list.json');
    cy.route(
      'GET',
      'http://localhost:8000/v1/monasteries/list?q=location.eq:köln&limit=30&offset=0',
      'fixture:list-location-koeln.json',
    );
    cy.route(
      'GET',
      'http://localhost:8000/v1/monasteries/list?q=location.eq:köln,aachen&limit=30&offset=0',
      'fixture:list-location-koeln-aachen.json',
    );

    cy.fixture('list-location-koeln.json').then((json) => {
      listKoelnJson = json;
    });
    cy.fixture('list-location-koeln-aachen.json').then((json) => {
      listKoelnAachenJson = json;
    });
    cy.visit('/monasteries/list');
  });

  it('loads the location search bar', () => {
    getSearchContainer().contains('Ort');
  });

  it('reveals the input when clicking into search bar', () => {
    clickInSearchBar();
    getLocationSearchBarContainer().find('input[type=text]').should('be.visible');
  });

  it('trigger search on with entered input', () => {
    clickInSearchBar();
    getLocationSearchBarContainer().find('input[type=text]').type('köln').type('{enter}');
    getSearchContainer().contains('Suchen').click();
    checkSearchResult(listKoelnJson.total);
  });

  it('trigger search on with multiple entered inputs', () => {
    clickInSearchBar();
    getLocationSearchBarContainer().find('input[type=text]').type('köln').type('{enter}');
    clickInSearchBar();
    getLocationSearchBarContainer().find('input[type=text]').type('aachen').type('{enter}');

    getSearchContainer().contains('Suchen').click();
    checkSearchResult(listKoelnAachenJson.total);
  });

  it('performs search field specific interactions', () => {
    // check click on suggestions
    clickInSearchBar();
    getLocationSearchBarContainer().find('input[type=text]').type('köln');
    getLocationSearchBarContainer().find('.multiselect__content').contains('köln').click();
    getLocationSearchBarContainer().find('.multiselect__tags').contains('köln');

    // check floating label
    getLocationSearchBarContainer().find('.floating-label').contains('Ort');

    // check reset
    getLocationSearchBarContainer().contains('zurücksetzen').click();
    getLocationSearchBarContainer().find('.multiselect__tags-wrap').should('be.empty');

    // check tag close button
    clickInSearchBar();
    getLocationSearchBarContainer().find('input[type=text]').type('köln').type('{enter}');
    getLocationSearchBarContainer().find('.multiselect__tags').contains('köln').siblings('.multiselect__tag-icon').click();
    getLocationSearchBarContainer().find('.multiselect__tags-wrap').should('be.empty');
  });
});
