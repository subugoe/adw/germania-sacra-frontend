function getFacetDiocese () {
  return cy.get('.facet-diocese');
}

function getFacetOrder () {
  return cy.get('.facet-order');
}

describe('Sidebar', () => {
  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/diocese?limit=30&offset=0', 'fixture:facet-diocese.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/order?limit=30&offset=0', 'fixture:facet-order.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/order?f=diocese.eq:Konstanz?limit=30&offset=0')
      .as('facetOrderFilteredByDioceseKonstanz');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/diocese?f=order.eq:Kanoniker?limit=30&offset=0')
      .as('facetDioceseFilteredByOrderKanoniker');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/diocese?f=diocese.eq:Konstanz;order.eq:Kanoniker?limit=30&offset=0')
      .as('facetDioceseMultipleFilters1');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/diocese?f=diocese.eq:Konstanz;order.eq:Kanoniker,Kapuziner?limit=30&offset=0')
      .as('facetDioceseMultipleFilters2');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?limit=30&offset=0', 'fixture:list.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=diocese.eq:Konstanz&limit=30&offset=0', 'fixture:list-konstanz.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=diocese.eq:Mainz&limit=30&offset=0', 'fixture:list-mainz.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=diocese.eq:Mainz,Konstanz&limit=30&offset=0', 'fixture:list-konstanz-mainz.json');
    cy.visit('/monasteries/list');
    cy.wait(500);
  });

  it('reload order facet after diocese facet click', () => {
    getFacetDiocese().contains('Konstanz').click();
    cy.wait('@facetOrderFilteredByDioceseKonstanz');
  });

  it('reload diocese facet after order facet click', () => {
    getFacetOrder().contains('Kanoniker').click();
    cy.wait('@facetDioceseFilteredByOrderKanoniker');
  });

  it('reload facets correctly after multiple selections', () => {
    getFacetDiocese().contains('Konstanz').click();
    cy.wait('@facetOrderFilteredByDioceseKonstanz');

    getFacetOrder().contains('Kanoniker').click();
    cy.wait('@facetDioceseMultipleFilters1');

    getFacetOrder().contains('Kapuziner').click();
    cy.wait('@facetDioceseMultipleFilters2');
  });
});
