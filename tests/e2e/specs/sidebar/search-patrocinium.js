function getSearchContainer () {
  return cy.get('.search-container');
}

function getPatrociniumSearchBarContainer () {
  return getSearchContainer().find('>div:nth-child(2)');
}

function clickInSearchBar () {
  return getPatrociniumSearchBarContainer().first().click();
}

function checkSearchResult (amount) {
  return cy.get('.content-container').contains('Suchergebnis').parent().contains(amount + ' Treffer');
}

describe('Search Patrocinium', () => {
  let listPetrusJson;
  let listPetrusMichaelJson;
  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?limit=30&offset=0', 'fixture:list.json');
    cy.route(
      'GET',
      'http://localhost:8000/v1/monasteries/list?q=patrocinium.eq:Petrus&limit=30&offset=0',
      'fixture:list-patrocinium-petrus.json',
    );
    cy.route(
      'GET',
      'http://localhost:8000/v1/monasteries/list?q=patrocinium.eq:Petrus,Michael&limit=30&offset=0',
      'fixture:list-patrocinium-petrus-michael.json',
    );

    cy.fixture('list-patrocinium-petrus.json').then((json) => {
      listPetrusJson = json;
    });
    cy.fixture('list-patrocinium-petrus-michael.json').then((json) => {
      listPetrusMichaelJson = json;
    });
    cy.visit('/monasteries/list');
  });

  it('loads the Patrocinium search bar', () => {
    getSearchContainer().contains('Patrozinium');
  });

  it('reveals the input when clicking into search bar', () => {
    clickInSearchBar();
    getPatrociniumSearchBarContainer().find('input[type=text]').should('be.visible');
  });

  it('trigger search on with entered input', () => {
    clickInSearchBar();
    getPatrociniumSearchBarContainer().find('input[type=text]').type('Petrus').type('{enter}');
    getSearchContainer().contains('Suchen').click();
    checkSearchResult(listPetrusJson.total);
  });

  // TODO: Fix Lowercase Issue on SOLR
  // it('trigger search on with entered input lowercase', () => {
  //   clickInSearchBar();
  //   getPatrociniumSearchBarContainer().find('input[type=text]').type('petrus').type('{enter}');
  //   getSearchContainer().contains('Suchen').click();
  //   checkSearchResult(listPetrusJson.total);
  // });

  it('trigger search on with multiple entered inputs', () => {
    clickInSearchBar();
    getPatrociniumSearchBarContainer().find('input[type=text]').type('Petrus').type('{enter}');
    clickInSearchBar();
    getPatrociniumSearchBarContainer().find('input[type=text]').type('Michael').type('{enter}');

    getSearchContainer().contains('Suchen').click();
    checkSearchResult(listPetrusMichaelJson.total);
  });

  it('performs search field specific interactions', () => {
    // check click on suggestions
    clickInSearchBar();
    getPatrociniumSearchBarContainer().find('input[type=text]').type('Petrus');
    getPatrociniumSearchBarContainer().find('.multiselect__content').contains('Petrus').click();
    getPatrociniumSearchBarContainer().find('.multiselect__tags').contains('Petrus');

    // check floating label
    getPatrociniumSearchBarContainer().find('.floating-label').contains('Patrozinium');

    // check reset
    getPatrociniumSearchBarContainer().contains('zurücksetzen').click();
    getPatrociniumSearchBarContainer().find('.multiselect__tags-wrap').should('be.empty');

    // check tag close button
    clickInSearchBar();
    getPatrociniumSearchBarContainer().find('input[type=text]').type('Petrus').type('{enter}');
    getPatrociniumSearchBarContainer().find('.multiselect__tags').contains('Petrus').siblings('.multiselect__tag-icon').click();
    getPatrociniumSearchBarContainer().find('.multiselect__tags-wrap').should('be.empty');
  });
});
