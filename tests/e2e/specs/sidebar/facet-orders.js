function getFacetOrder () {
  return cy.get('.facet-order');
}

function getFacetOrderList () {
  return getFacetOrder().find('.multiselect__content');
}

function getFacetOrderSearch () {
  return getFacetOrder().find('.multiselect__tags');
}

function checkSearchResult (amount) {
  return cy.get('.content-container').contains('Suchergebnis').parent().contains(amount + ' Treffer');
}

describe('Facet order', () => {
  let listKanonikerJson;

  before(() => {
    cy.fixture('list-order-kanoniker.json').then((json) => {
      listKanonikerJson = json;
    });
  });

  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/order?limit=30&offset=0', 'fixture:facet-order.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?limit=30&offset=0', 'fixture:list.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=order.eq:Kanoniker&limit=30&offset=0', 'fixture:list-order-kanoniker.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=order.eq:Benediktiner&limit=30&offset=0', 'fixture:list-order-benediktiner.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=order.eq:Benediktiner,Kanoniker&limit=30&offset=0', 'fixture:list-order-benediktiner-kanoniker.json');
    cy.visit('/monasteries/list');
    cy.wait(500);
  });

  it('loads the order facet', () => {
    cy.get('.sidebar-container').contains('Orden');
  });

  it('checks order icons', () => {
    getFacetOrderList().find('.multiselect__element').each(el => {
      cy.wrap(el).find('img');
    });
  });

  it('checks multiple selection', () => {
    getFacetOrder().contains('Kanoniker').parent().parent().click().should('have.class', 'multiselect__option--selected');
    getFacetOrderSearch().contains('Kanoniker');
    getFacetOrder().contains('Benediktiner').parent().parent().click().should('have.class', 'multiselect__option--selected');
    getFacetOrderList().contains('Kanoniker').parent().parent().should('have.class', 'multiselect__option--selected');
    getFacetOrderSearch().contains('Benediktiner');
    getFacetOrderSearch().contains('Benediktiner').siblings('.multiselect__tag-icon').click();
    getFacetOrderSearch().contains('Benediktiner').should('not.exist');
    getFacetOrderList().contains('Benediktiner').parent().parent().should('not.have.class', 'multiselect__option--selected');
    getFacetOrderSearch().contains('Kanoniker').siblings('.multiselect__tag-icon').click();
    getFacetOrderSearch().contains('Kanoniker').should('not.exist');
    getFacetOrderList().contains('Kanoniker').parent().parent().should('not.have.class', 'multiselect__option--selected');
    getFacetOrderSearch().find('.multiselect__tags-wrap').should('not.be.visible');
  });

  it('triggers request on checkbox click', () => {
    getFacetOrder().contains('Kanoniker').parent().parent().click();
    getFacetOrder().get('.el-loading-spinner').should('be.visible');
    checkSearchResult(listKanonikerJson.total);
  });

  it('checks if a list exist ', () => {
    getFacetOrderList().find('.multiselect__element').should('have.length', 80);
  });

  it('loads the order facet by search', () => {
    getFacetOrderSearch().find('input[type=text]').type('Kanon');
    getFacetOrderList().find('.multiselect__element').should('not.have.length', 80);
  });
  it('resets selection ', () => {
    getFacetOrder().contains('Kanoniker').parent().parent().click();
    getFacetOrderSearch().contains('Kanoniker');
    getFacetOrderList().contains('Benediktiner').parent().parent().click();
    getFacetOrderSearch().contains('Benediktiner');
    getFacetOrder().contains('zurücksetzen').click();
    getFacetOrderSearch().find('input[type=text]').should('have.value', '');
    getFacetOrderList().contains('Kanoniker').parent().parent().should('not.have.class', 'multiselect__option--selected');
    getFacetOrderList().contains('Benediktiner').parent().parent().should('not.have.class', 'multiselect__option--selected');
  });

  it('handles collapse and expand', () => {
    getFacetOrderList().find('.multiselect__element').each((el, index) => {
      const wrapped = cy.wrap(el);
      wrapped.scrollIntoView();
      if (index <= 7) wrapped.should('be.visible');
      else wrapped.should('not.be.visible');
    });

    getFacetOrder().contains('73 weitere anzeigen').click();

    getFacetOrderList().find('.multiselect__element').each(el => {
      cy.wrap(el).scrollIntoView().should('be.visible');
    });

    getFacetOrder().contains('zuklappen').click();

    getFacetOrderList().find('.multiselect__element').each((el, index) => {
      const wrapped = cy.wrap(el);
      wrapped.scrollIntoView();
      if (index <= 7) wrapped.should('be.visible');
      else wrapped.should('not.be.visible');
    });

    getFacetOrder().contains('73 weitere anzeigen');
  });

  it('handles instant search', () => {
    getFacetOrderSearch().find('input[type=text]').type('h');
    getFacetOrderList().find('.multiselect__element').should('have.length', 29);
    getFacetOrder().contains('22 weitere anzeigen');
    getFacetOrderSearch().find('input[type=text]').type('asdasda');
    getFacetOrderList().find('.multiselect__element').should('have.length', 0);
  });
});

describe('Facet order less than 8', () => {
  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/order?limit=30&offset=0', 'fixture:facet-order-lite.json');
    cy.visit('/monasteries/list');
  });

  it('checks if a list exist ', () => {
    getFacetOrderList().find('.multiselect__element').should('have.length', 7);
  });

  it('handles collapse and expand', () => {
    getFacetOrder().contains('weitere anzeigen').should('not.exist');

    getFacetOrderList().find('.multiselect__element').each(el => {
      cy.wrap(el).scrollIntoView().should('be.visible');
    });

    getFacetOrder().contains('zuklappen').should('not.exist');
  });

  it('handles instant search', () => {
    getFacetOrderSearch().scrollIntoView().find('input[type=text]').should('be.visible');
    getFacetOrderSearch().find('input[type=text]').type('k');
    getFacetOrderList().find('.multiselect__element').should('have.length', 6);
    getFacetOrderSearch().find('input[type=text]').type('asdasda');
    getFacetOrderList().find('.multiselect__element').should('have.length', 0);
  });
});
