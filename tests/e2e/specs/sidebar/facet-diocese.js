function getFacetDiocese () {
  return cy.get('.facet-diocese');
}

function getFacetDioceseList () {
  return getFacetDiocese().find('.multiselect__content');
}

function getFacetDioceseSearch () {
  return getFacetDiocese().find('.multiselect__tags');
}

function checkSearchResult (amount) {
  return cy.get('.content-container').contains('Suchergebnis').parent().contains(amount + ' Treffer');
}

describe('Facet diocese', () => {
  let listKonstanzJson;

  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/diocese?limit=30&offset=0', 'fixture:facet-diocese.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?limit=30&offset=0', 'fixture:list.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=diocese.eq:Konstanz&limit=30&offset=0', 'fixture:list-konstanz.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=diocese.eq:Mainz&limit=30&offset=0', 'fixture:list-mainz.json');
    cy.route('GET', 'http://localhost:8000/v1/monasteries/list?f=diocese.eq:Mainz,Konstanz&limit=30&offset=0', 'fixture:list-konstanz-mainz.json');
    cy.fixture('list-konstanz.json').then((json) => {
      listKonstanzJson = json;
    });
    cy.visit('/monasteries/list');
    cy.wait(500);
  });

  it('loads the diocese facet', () => {
    cy.get('.sidebar-container').contains('Bistümer');
  });

  it('checks multiple selection', () => {
    // if konstanz checkbox visible
    // click on konstanz checkbox
    // checkbox is checked
    getFacetDiocese().contains('Konstanz').click().parent().parent().should('have.class', 'multiselect__option--selected');

    // tag appear in searchbar (konstanz)
    getFacetDioceseSearch().contains('Konstanz');

    // click again mainz checkbox
    // mainz checkbox is checked
    getFacetDiocese().contains('Mainz').click().parent().parent().should('have.class', 'multiselect__option--selected');

    // konstanz checkbox is checked
    getFacetDioceseList().contains('Konstanz').parent().parent().should('have.class', 'multiselect__option--selected');

    // mainz tag also appear in searchbar
    getFacetDioceseSearch().contains('Mainz');

    // click the close button on mainz tag
    getFacetDioceseSearch().contains('Mainz').siblings('.multiselect__tag-icon').click();

    // mainz tag should disappear
    getFacetDioceseSearch().contains('Mainz').should('not.exist');

    // mainz checkbox should be unchecked
    getFacetDioceseList().contains('Mainz').parent().parent().should('not.have.class', 'multiselect__option--selected');

    // click the close button on konstan tag
    getFacetDioceseSearch().contains('Konstanz').siblings('.multiselect__tag-icon').click();

    // konstanz tag should disappear
    getFacetDioceseSearch().contains('Konstanz').should('not.exist');

    // konstanz checkbox should be unchecked
    getFacetDioceseList().contains('Konstanz').parent().parent().should('not.have.class', 'multiselect__option--selected');

    // search bar should be cleaned
    getFacetDioceseSearch().find('.multiselect__tags-wrap').should('not.be.visible');
  });

  it('triggers request on checkbox click', () => {
    getFacetDiocese().contains('Konstanz').parent().parent().click();
    getFacetDiocese().get('.el-loading-spinner').should('be.visible');
    checkSearchResult(listKonstanzJson.total);
  });

  it('checks if a list exist ', () => {
    getFacetDioceseList().find('.multiselect__element').should('have.length', 65);
  });

  it('loads the diocese facet by search', () => {
    // check search input is visible
    // type search letters
    getFacetDioceseSearch().find('input[type=text]').should('be.visible').type('Konst');

    // check if bistumer(diocese) list changes
    getFacetDioceseList().find('.multiselect__element').should('not.have.length', 65);
  });
  it('resets selection ', () => {
    // if konstanz checkbox visible
    getFacetDioceseList().contains('Konstanz').parent().parent().should('be.visible');

    // click on konstanz checkbox
    getFacetDiocese().contains('Konstanz').parent().parent().click();

    // tag appear in searchbar (konstanz)
    getFacetDioceseSearch().contains('Konstanz');

    // click again mainz checkbox
    getFacetDioceseList().contains('Mainz').parent().parent().click();

    // mainz tag also appear in searchbar
    getFacetDioceseSearch().contains('Mainz');

    // reset button should be visible
    // click reset
    getFacetDiocese().contains('zurücksetzen').click();

    // search bar clear
    getFacetDioceseSearch().find('input[type=text]').should('have.value', '');

    // all checkboxes unchecked
    getFacetDioceseList().contains('Konstanz').parent().parent().should('not.have.class', 'multiselect__option--selected');
    getFacetDioceseList().contains('Mainz').parent().parent().should('not.have.class', 'multiselect__option--selected');
  });

  it('handles collapse and expand', () => {
    getFacetDioceseList().find('.multiselect__element').each((el, index) => {
      if (index <= 7) {
        // check first 7/65 visible
        cy.wrap(el).should('be.visible');
      } else {
        // check other 58/65 not visible
        cy.wrap(el).should('not.be.visible');
      }
    });

    // check expand button have expand text(58 weitere anzeigen) and visible
    // click expand button
    getFacetDiocese().contains('58 weitere anzeigen').click();

    // all 65/65 should be visible on the list
    getFacetDioceseList().find('.multiselect__element').should('be.visible');

    // button text should be collapse text zuklappen
    // click on zuklappen button
    getFacetDiocese().contains('zuklappen').click();

    // TODO: find out why it thinks everything is hidden
    // getFacetDioceseList().find('.multiselect__element').each((el, index) => {
    //   if (index <= 7) {
    //     // check first 7/65 visible
    //     cy.wrap(el).should('be.visible');
    //   } else {
    //     // check other 58/65 not visible
    //     cy.wrap(el).should('not.be.visible');
    //   }
    // });
    getFacetDiocese().contains('58 weitere anzeigen');
  });

  it('handles instant search', () => {
    // check search is visible
    getFacetDioceseSearch().find('input[type=text]').should('be.visible');

    // type search letters
    getFacetDioceseSearch().find('input[type=text]').type('h');

    // check if bistumer(diocese) list changes
    getFacetDioceseList().find('.multiselect__element').should('have.length', 11);

    // check if the number of collapsed items changed
    getFacetDiocese().contains('4 weitere anzeigen');

    // type asdasda on search and list should have no entries aka keine treffer text
    getFacetDioceseSearch().find('input[type=text]').type('asdasda');
    getFacetDioceseList().find('.multiselect__element').should('have.length', 0);
  });
});

// facet with less than 8 entries search, number updates, collapse
describe('Facet diocese less than 8', () => {
  beforeEach(() => {
    cy.server();
    cy.route('GET', 'http://localhost:8000/v1/monasteries/facet/diocese?limit=30&offset=0', 'fixture:facet-diocese-lite.json');
    cy.visit('/monasteries/list');
  });

  it('checks if a list exist ', () => {
    getFacetDioceseList().find('.multiselect__element').should('have.length', 7);
  });

  it('handles collapse and expand', () => {
    // check expand button have expand text(58 weitere anzeigen) and visible
    // click expand button
    getFacetDiocese().contains('weitere anzeigen').should('not.exist');

    // all 65/65 should be visible on the list
    getFacetDioceseList().find('.multiselect__element').should('be.visible');

    // button text should be collapse text zuklappen
    // click on zuklappen button
    getFacetDiocese().contains('zuklappen').should('not.exist');
  });

  it('handles instant search', () => {
    // check search is visible
    getFacetDioceseSearch().find('input[type=text]').should('be.visible');

    // type search letters
    getFacetDioceseSearch().find('input[type=text]').type('k');

    // check if bistumer(diocese) list changes
    getFacetDioceseList().find('.multiselect__element').should('have.length', 2);

    // type asdasda on search and list should have no entries aka keine treffer text
    getFacetDioceseSearch().find('input[type=text]').type('asdasda');
    getFacetDioceseList().find('.multiselect__element').should('have.length', 0);
  });
});
