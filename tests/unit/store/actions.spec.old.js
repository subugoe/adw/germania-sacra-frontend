// import { actions } from '@/store';
// import types from '@/store/mutation-types';
// import apiService from '@/store/api';
//
// // const mockApiService = () => {
// //   return {
// //     getMonasteriesList () {
// //       return Promise.resolve({
// //         list: [],
// //         total: 0,
// //       });
// //     },
// //   };
// // };
// jest.mock('@/store/api');
//
// const mockResponse = {
//   list: [],
//   total: 0,
// };
//
// apiService.getMonasteriesList.mockResolvedValue(mockResponse);
//
// function getFullQuery (search = null, facets = null, offset = 0) {
//   return { facets: facets, search: search, offset: offset };
// }
//
// describe('Store actions', () => {
//   const commit = jest.fn();
//   const dispatch = jest.fn();
//   let state = {
//     view: 'list',
//   };
//
//   const query = [{
//     attribute: 'location',
//     value: 'Aachen',
//     relation: 'eq',
//   }];
//
//   afterEach(() => {
//     jest.clearAllMocks();
//   });
//
//   test('search with view:list', () => {
//     actions.search({ commit, dispatch, state }, query);
//
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_PLACES, false);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_LIST, false);
//
//     expect(dispatch).toHaveBeenCalledWith('setQuery', getFullQuery(query));
//     expect(dispatch).toHaveBeenCalledWith('getMonasteriesList');
//     expect(dispatch).toHaveBeenCalledWith('getMonasteriesFacets');
//     expect(dispatch).toHaveBeenCalledWith('getHistogram');
//   });
//
//   test('search with view:map', () => {
//     state = {
//       view: 'map',
//     };
//     actions.search({ commit, dispatch, state }, query);
//
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_PLACES, false);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_LIST, false);
//
//     expect(dispatch).toHaveBeenCalledWith('setQuery', getFullQuery(query));
//     expect(dispatch).toHaveBeenCalledWith('getMonasteriesLocations');
//     expect(dispatch).toHaveBeenCalledWith('getMonasteriesFacets');
//     expect(dispatch).toHaveBeenCalledWith('getHistogram');
//   });
//
//   test('applyFacet view:list', () => {
//     const values = ['CheckedValue1', 'CheckedValue2'];
//     const index = 0;
//
//     state = {
//       view: 'list',
//       facets: [{
//         active: values,
//         attribute: 'testattr',
//       }],
//     };
//
//     const facetsQuery = [{
//       attribute: 'testattr',
//       value: values.join(','),
//       relation: 'eq',
//     }];
//     actions.applyFacet({ commit, dispatch, state }, { values, index });
//
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_PLACES, false);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_LIST, false);
//
//     expect(dispatch).toHaveBeenCalledWith('setQuery', { facets: facetsQuery, offset: 0 });
//     expect(dispatch).toHaveBeenCalledWith('getMonasteriesList');
//   });
//
//   test('applyFacet view:map', () => {
//     const values = ['CheckedValue1', 'CheckedValue2'];
//     const index = 0;
//
//     state = {
//       view: 'map',
//       facets: [{
//         active: values,
//         attribute: 'testattr',
//       }],
//     };
//
//     const facetsQuery = [{
//       attribute: 'testattr',
//       value: values.join(','),
//       relation: 'eq',
//     }];
//     actions.applyFacet({ commit, dispatch, state }, { values, index });
//
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_PLACES, false);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_LIST, false);
//
//     expect(dispatch).toHaveBeenCalledWith('setQuery', { facets: facetsQuery, offset: 0 });
//     expect(dispatch).toHaveBeenCalledWith('getMonasteriesLocations');
//   });
//
//   test('getMonasteriesList with query', async () => {
//     const state = {
//       monasteriesQuery: {
//         facets: [{
//           attribute: 'testattr',
//           value: 'Value1',
//           relation: 'eq',
//         }],
//         search: [{
//           attribute: 'testattr2',
//           value: 'Value2',
//           relation: 'eq',
//         }],
//         offset: 0,
//         limit: 100,
//       },
//     };
//
//     const response = {
//       list: [],
//       total: 0,
//     };
//
//     await actions.getMonasteriesList({ commit, dispatch, state }, query);
//
//     expect(dispatch).toHaveBeenCalledWith('setQuery', query);
//     expect(apiService.getMonasteriesList).toHaveBeenCalledWith({
//       q: 'testattr2.eq:Value2',
//       f: 'testattr.eq:Value1',
//       limit: 100,
//       offset: 0,
//     });
//
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADING_LIST, true);
//     expect(commit).toHaveBeenCalledWith(types.SET_MONASTERIES_LIST, response.list);
//
//     expect(commit).toHaveBeenCalledWith(types.SET_MONASTERIES_LIST_TOTAL_NUMBER, response.total);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADING_LIST, false);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_LIST, true);
//   });
//
//   test('getMonasteriesList no query', async () => {
//     const state = {
//       monasteriesQuery: {
//         facets: [{
//           attribute: 'testattr',
//           value: 'Value1',
//           relation: 'eq',
//         }],
//         search: [{
//           attribute: 'testattr2',
//           value: 'Value2',
//           relation: 'eq',
//         }],
//         offset: 0,
//         limit: 100,
//       },
//     };
//
//     const response = {
//       list: [],
//       total: 0,
//     };
//
//     await actions.getMonasteriesList({ commit, dispatch, state });
//
//     expect(dispatch).toHaveBeenCalledTimes(0);
//     expect(apiService.getMonasteriesList).toHaveBeenCalledWith({
//       q: 'testattr2.eq:Value2',
//       f: 'testattr.eq:Value1',
//       limit: 100,
//       offset: 0,
//     });
//
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_LIST, true);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADING_LIST, true);
//     expect(commit).toHaveBeenCalledWith(types.SET_MONASTERIES_LIST, response.list);
//
//     expect(commit).toHaveBeenCalledWith(types.SET_MONASTERIES_LIST_TOTAL_NUMBER, response.total);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADING_LIST, false);
//     expect(commit).toHaveBeenCalledWith(types.SET_LOADED_LIST, true);
//   });
// });
