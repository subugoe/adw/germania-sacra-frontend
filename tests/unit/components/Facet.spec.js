import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import Facet from '@/components/Facet.vue';
import FacetDioceseJson from 'tests/e2e/fixtures/facet-diocese.json';
import { Loading } from 'element-ui';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Loading.directive);

const Multiselect = {
  template: '<div />',
  data () {
    return {
      isOpen: true,
    };
  },
};

const stubs = {
  multiselect: Multiselect,
  'el-checkbox-group': true,
  'el-checkbox': true,
  'el-button': true,
};

const facet = FacetDioceseJson;

function factory () {
  const store = new Vuex.Store({
    actions: {},
    state: {},
  });

  return shallowMount(Facet, {
    store,
    localVue,
    stubs,
    computed: {
      loading: () => true,
    },
  });
}

describe('Facet blank', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = factory();
  });

  it('mounts', () => {
    expect(wrapper).toBeTruthy();
  });
});

describe('Facet with data', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = factory();
    wrapper.setProps({ facet: facet });
  });

  it('loads facet data', () => {
    expect(wrapper.vm.filteredFields.length).toEqual(65);
    expect(wrapper.vm.filteredFieldsAmount).toEqual(65);
  });

  it('sets selected from facet data', async () => {
    const facetWithActive = { ...facet, active: ['Konstanz'] };
    wrapper.setProps({ facet: facetWithActive });
    await Vue.nextTick();
    expect(wrapper.vm.selected).toEqual([{ label: facetWithActive.active[0], term: facetWithActive.active[0] }]);
    expect(wrapper.vm.filteredFields[0].selected).toEqual(true);
  });

  it('handles checkbox change', async () => {
    const term = facet.values[0].term;
    await wrapper.setData({ selectedCheckboxes: [term] });

    wrapper.vm.onCheckboxChange();
    expect(wrapper.vm.selected).toEqual([{ label: term, term }]);

    await Vue.nextTick();

    expect(wrapper.emitted().change[0][0]).toEqual([term]);
  });

  it('handles select', () => {
    const term = facet.values[0].term;
    expect(wrapper.vm.filteredFields[0].selected).toEqual(false);
    wrapper.vm.onSelect({ label: term, term });
    expect(wrapper.vm.filteredFields[0].selected).toEqual(true);
  });

  it('handles selected', () => {
    const term = facet.values[0].term;
    const selected = [{ label: term, term }];
    wrapper.vm.onSelected(selected);
    expect(wrapper.vm.selected).toEqual(selected);
    expect(wrapper.emitted().change[0][0]).toEqual([term]);
  });

  it('handles change', () => {
    wrapper.vm.onChange(5);
    expect(wrapper.vm.filteredFieldsAmount).toEqual(5);
    expect(wrapper.vm.isCollapsed).toEqual(false);

    wrapper.vm.onChange(20);
    expect(wrapper.vm.filteredFieldsAmount).toEqual(20);
    expect(wrapper.vm.isCollapsed).toEqual(true);
  });

  it('handles remove', () => {
    const term = facet.values[0].term;
    wrapper.vm.filteredFields[0].selected = true;
    wrapper.vm.onRemove({ label: term, term });
    expect(wrapper.vm.filteredFields[0].selected).toEqual(false);
  });

  it('handles collapse toggle', () => {
    wrapper.vm.onCollapseToggle();
    expect(wrapper.vm.isCollapsed).toEqual(false);

    wrapper.vm.onCollapseToggle();
    expect(wrapper.vm.isCollapsed).toEqual(true);
  });

  it('handles reset', () => {
    wrapper.vm.selected = [{ label: 'Test', value: 'Test' }];
    wrapper.vm.filteredFields[0].selected = true;
    wrapper.vm.reset();
    expect(wrapper.vm.selected).toEqual([]);
    expect(wrapper.vm.filteredFields[0].selected).toEqual(false);
    expect(wrapper.emitted().change[0][0]).toEqual([]);
  });
});
