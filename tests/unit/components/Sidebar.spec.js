import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import Sidebar from '@/components/Sidebar';
import { Loading } from 'element-ui';
import FacetDioceseJson from 'tests/e2e/fixtures/facet-diocese.json';
import FacetOrderJson from 'tests/e2e/fixtures/facet-order.json';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Loading.directive);

const store = new Vuex.Store({
  actions: {},
  state: {
    facetDiocese: FacetDioceseJson,
    facetOrder: FacetOrderJson,
    loadingFacets: false,
    monasteriesQuery: {
      search: null,
      facetDiocese: null,
      facetOrder: null,
      facetGender: null,
    },
  },
});
store.dispatch = jest.fn();

const stubs = ['SearchInput', 'SearchFieldGroup', 'Facet', 'OrderIcon', 'Histogram', 'el-button'];

function factory () {
  return shallowMount(Sidebar, {
    store,
    localVue,
    stubs,
  });
}

describe('Sidebar.vue', () => {
  const wrapper = factory();
  const emptySearchGroup = {
    field: '',
    value: [],
    operator: '',
  };
  const attributeName = 'testattr';
  const values = ['ValueOne', 'ValueTwo'];

  it('mounts', () => {
    expect(wrapper).toBeTruthy();
  });

  it('handles search event', () => {
    wrapper.vm.onSearch(attributeName, values);
    expect(wrapper.vm.searchParams).toHaveProperty(attributeName);
    expect(wrapper.vm.searchParams[attributeName]).toEqual(values);

    wrapper.vm.onSearch(attributeName, []);
    expect(wrapper.vm.searchParams[attributeName]).toEqual(null);
  });

  it('executes search', () => {
    wrapper.vm.searchParams = {
      [attributeName]: values,
    };

    wrapper.vm.search(attributeName, values);
    expect(store.dispatch)
      .toHaveBeenCalledWith(
        'search',
        [
          { field: attributeName, value: values[0], operator: 'eq' },
          { field: attributeName, value: values[1], operator: 'eq' },
        ]);
  });

  it('adds new advanced search fields param', () => {
    wrapper.vm.addNewParam();
    expect(wrapper.vm.searchGroups).toHaveLength(1);
  });

  it('handles search group change', () => {
    wrapper.vm.searchGroupsParams = [emptySearchGroup];
    const changedGroup = {
      field: attributeName,
      operator: 'eq',
      value: values[0],
    };
    wrapper.vm.onGroupChange(changedGroup, 0);
    expect(wrapper.vm.searchGroupsParams).toEqual([changedGroup]);
  });
  it('handles search group remove', () => {
    wrapper.vm.searchGroupsParams = [emptySearchGroup];
    wrapper.vm.onGroupRemove(0);
    expect(wrapper.vm.searchGroupsParams).toEqual([]);
  });

  it('sets facets correctly', () => {
    expect(wrapper.vm.facetDiocese).toEqual(FacetDioceseJson);
    expect(wrapper.vm.facetOrder).toEqual(FacetOrderJson);
    // expect(wrapper.vm.genderFacet).toEqual(FacetGenderJson);
  });

  it('handles facet change', () => {
    const checkedList = ['CheckedValue1', 'CheckedValue2'];
    const field = 'diocese';
    wrapper.vm.onFacetChange(checkedList, field);
    expect(store.dispatch)
      .toHaveBeenCalledWith(
        'applyFacet',
        {
          values: checkedList,
          field,
        });
  });

  // TODO: adjust correct payload in comp first
  // it('handles histogram filter change', () => {
  //   const checkedList = ['CheckedValue1', 'CheckedValue2'];
  //   const filterData = {
  //     from: 1000,
  //     to: 1500,
  //   };
  //   wrapper.vm.onHistogramFilter(filterData);
  //   expect(store.dispatch)
  //     .toHaveBeenCalledWith(
  //       'search'
  //     );
  // });

  //
  // it('sets view from route map', async () => {
  //   await router.push('/monasteries/map');
  //   wrapper = factory();
  //   await wrapper.vm.$nextTick();
  //   expect(wrapper.vm.viewSwitch).toEqual('map');
  // });
  //
  // it('sets view from route list', async () => {
  //   await router.push('/monasteries/list');
  //   wrapper = factory();
  //   await wrapper.vm.$nextTick();
  //   expect(wrapper.vm.viewSwitch).toEqual('list');
  // });
});
