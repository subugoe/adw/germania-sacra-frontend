import { createLocalVue, shallowMount } from '@vue/test-utils';
import SearchInput from '@/components/SearchInput.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();

localVue.use(Vuex);

const stubs = ['multiselect', 'el-button'];

function factory () {
  const store = new Vuex.Store({
    actions: {},
    state: {
      monasteriesQuery: {
        search: null,
        facetDiocese: null,
        facetOrder: null,
        facetGender: null,
      },
    },
  });
  return shallowMount(SearchInput, {
    store,
    localVue,
    stubs,
  });
}

describe('SearchInput', () => {
  let wrapper;
  let emitSearchSpy;

  beforeEach(() => {
    emitSearchSpy = jest.spyOn(SearchInput.methods, 'emitSearch');
    wrapper = factory();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('mounts', () => {
    expect(wrapper.vm).toBeTruthy();
  });

  it('emits search', () => {
    wrapper.vm.emitSearch();
    expect(wrapper.emitted().search[0][0]).toEqual([]);
    expect(wrapper.vm.$data.options).toEqual([]);

    wrapper.setData({
      selected: [{ name: 'aachen', value: 'aachen' }],
    });
    wrapper.vm.emitSearch();

    expect(wrapper.emitted().search[1][0]).toEqual(['aachen']);
    expect(wrapper.vm.$data.options).toEqual([]);
  });

  it('handles on input', () => {
    wrapper.vm.onInput([]);
    expect(emitSearchSpy).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.$data.showFloatingLabel).toEqual(false);

    emitSearchSpy.mockClear();

    wrapper.vm.onInput([{ name: 'aachen', value: 'aachen' }]);
    expect(emitSearchSpy).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.$data.showFloatingLabel).toEqual(true);
  });

  it('handles on tag', () => {
    wrapper.vm.onTag('aachen');
    expect(wrapper.vm.$data.selected).toEqual([{ name: 'aachen', value: 'aachen' }]);
    expect(wrapper.vm.$data.showFloatingLabel).toEqual(true);

    wrapper.vm.onTag('köln');
    expect(wrapper.vm.$data.selected).toEqual([{ name: 'aachen', value: 'aachen' }, { name: 'köln', value: 'köln' }]);
    expect(wrapper.vm.$data.showFloatingLabel).toEqual(true);
  });

  it('handles reset click', () => {
    wrapper.vm.onResetClick();
    expect(wrapper.vm.$data.selected).toEqual([]);
    expect(wrapper.vm.$data.showFloatingLabel).toEqual(false);
    expect(wrapper.emitted().search[0][0]).toEqual([]);
  });
});
